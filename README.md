# LaTeX Vorlagen

Größtenteils inspiriert von [Phil Steinhorst](https://zivgitlab.uni-muenster.de/ag-vahrenhold/public/latex-templates). Ganz in seinem Sinne dürfen die Vorlagen frei verwendet, weitergegeben oder angepasst werden.

## Visual Studio Code

Ich arbeite mit Visual Studio Code und der Erweiterung LaTeX Workshop. In `.vscode/settings.json` habe ich einige Dateiendungen zu Dateien eingetragen die von LaTeX automatisch erstellt werden, für mich aber uninteressant sind.